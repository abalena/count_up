var interval = 75;
var startDate = new Date(Date.UTC(2022, 2, 24, 0, 0, 0));
var defaultDeadRus = 15600;
var audioUrl = './media/AUD-20220315-WA0001.mp3'

var date = new Date();
var now_utc =  Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
var animationDuration = 3000;
var frameDuration = 1000 / 60;
var totalFrames = Math.round( animationDuration / frameDuration );
var countTo = defaultDeadRus + Math.floor((now_utc - startDate) / (2000 * interval));
var element = document.querySelector( '.countup' );
var button = document.querySelector( '#learn-more' );
    
function easeOutQuad(t){
    return t * ( 2 - t )
}

function animateCountUp(el, countUp ){
	var frame = 0;
	var counter = setInterval(function(){
		frame++;
		var progress = easeOutQuad( frame / totalFrames );
		var currentCount = Math.round( countTo * progress );
		if ( parseInt( el.innerHTML, 10 ) !== currentCount ) {
			el.innerHTML = currentCount;
		}
		if ( frame === totalFrames ) {  
            el.style.textShadow = "rgb(0, 0, 0) 0px 0px  40px";
            document.onmousemove = function(e){
                setShadow(e);
            }
            clearInterval( counter );
		}
	}, frameDuration );
    countUp();
};

function countUp(){
    setInterval(function(){
        countTo++;
        element.innerHTML = countTo;
    }, interval * 1000)  
};

function playAudio(){
    var audio = new Audio(audioUrl);
    audio.volume = .1;
    document.onclick = function(){
        audio.play();
    } 
};

function setShadow(e){
    var tx = (innerWidth/2 - e.x)/50;
    var ty = (innerHeight/2 - e.y)/40;
    element.style.textShadow = "rgb(0, 0, 0) " + tx + "px " + ty + "px  40px";
}

window.onload = function() {
    animateCountUp(element, countUp);
    playAudio();

    button.onclick = function(){
        window.open("https://wallevidence.mizhvukhamy.com/","_blank");
    }
};
